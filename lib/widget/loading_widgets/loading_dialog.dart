import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:sizer/sizer.dart';
import '../../App/Paint/Cubit/cubit.dart';
import '../../constant/appColor.dart';

showLoadingDialog(BuildContext context) {
  var isDarkTheme = BlocProvider.of<ThemeCubit>(context).isDark;

  AlertDialog alertDialog = AlertDialog(
    backgroundColor: isDarkTheme ? Colors.grey.shade900 : Colors.white,
    content: Row(
      children: [
        SizedBox(
          height: 5.h,
          width: 10.w,
          child: const LoadingIndicator(
            indicatorType: Indicator.ballRotateChase,
            colors: [AppColor.primaryColor],
            strokeWidth: 4,
            backgroundColor: Colors.transparent,
            pathBackgroundColor: Colors.transparent,
          ),
        ),
        SizedBox(
          width: 2.w,
        ),
        Text(
          "الرجاء الانتظار ...",
          textDirection: TextDirection.rtl,
          style: TextStyle(
            fontSize: 14.sp,
            fontWeight: FontWeight.bold,
            color: isDarkTheme ? Colors.white : Colors.black,
          ),
        ),
      ],
    ),
  );
  showDialog(
    barrierDismissible: false,
    useSafeArea: true,
    context: context,
    builder: (BuildContext context) {
      return alertDialog;
    },
  );
}
