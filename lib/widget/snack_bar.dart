import 'package:flutter/material.dart';

showSnackBar(String message, BuildContext context, {Color color = Colors.red}) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Center(
        child: Text(
          message,
          textDirection: TextDirection.rtl,
          textAlign: TextAlign.center,
        ),
      ),
      backgroundColor: color,
      duration: const Duration(seconds: 3),
    ),
  );
}
