import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

// Constant
import '../constant/appColor.dart';

class TryAgainUi extends StatelessWidget {
  final VoidCallback onPressed;

  const TryAgainUi(this.onPressed);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(Icons.error_outline, size: 15.w),
          SizedBox(height: 4.h),
          ElevatedButton(
            onPressed: onPressed,
            style: ElevatedButton.styleFrom(
              fixedSize: Size.fromWidth(40.w),
              backgroundColor: AppColor.primaryColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            child: const Text(
              'اعادة المحاولة',
              style: TextStyle(
                fontFamily: 'Tajawal',
                fontWeight: FontWeight.w400,
                fontSize: 14,
                color: Colors.white,
              ),
            ),
          )
        ],
      ),
    );
  }
}
