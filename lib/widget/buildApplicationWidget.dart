import 'package:easy_url_launcher/easy_url_launcher.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:servizio/App/Applications/Pages/detailsApp.dart';
import 'package:servizio/componant.dart';
import 'package:servizio/utils/global_functions.dart';
import 'package:sizer/sizer.dart';
import 'package:shimmer_animation/shimmer_animation.dart';

import '../App/Home/models/app_model.dart';
import '../App/Paint/Cubit/cubit.dart';

class BuildAppWidget extends StatelessWidget {
  final App app;

  BuildAppWidget({super.key, required this.app});

  @override
  Widget build(BuildContext context) {
    var isDarkTheme = BlocProvider.of<ThemeCubit>(context).isDark;

    return GestureDetector(
      onTap: () {
        General.routingPage(
          context,
          DetailsAppScreen(app: app),
        );
      },
      child: Row(
        children: [
          Container(
            height: 8.h,
            width: 15.w,
            margin: EdgeInsets.symmetric(vertical: 1.h),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(2.w),
              child: Image.network(
                app.icon,
                fit: BoxFit.cover,
                errorBuilder: (context, error, stackTrace) =>
                    const Icon(Icons.error_outline),
              ),
            ),
          ),
          const Spacer(flex: 1),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              reusableText(
                text: app.name,
                style: TextStyle(
                  fontFamily: 'Tajawal',
                  fontWeight: FontWeight.w500,
                  fontSize: 12,
                  color: isDarkTheme ? Colors.white : Colors.black,
                ),
              ),
              SizedBox(
                width: 50.w,
                child: reusableText(
                  text: app.description,
                  overFlowType: TextOverflow.ellipsis,
                  maxLines: 1,
                  style: TextStyle(
                    fontFamily: 'Tajawal',
                    fontWeight: FontWeight.w400,
                    fontSize: 12,
                    color: isDarkTheme ? Colors.white : Colors.black,
                  ),
                ),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  reusableText(
                    text: app.sizeFile,
                    textDirection: TextDirection.ltr,
                    style: TextStyle(
                      fontFamily: 'Tajawal',
                      fontWeight: FontWeight.w400,
                      fontSize: 12,
                      color: isDarkTheme ? Colors.white : Colors.black,
                    ),
                  ),
                  SizedBox(
                    width: 3.w,
                  ),
                  reusableText(
                    text: app.views,
                    style: TextStyle(
                      fontFamily: 'Tajawal',
                      fontWeight: FontWeight.w400,
                      fontSize: 12,
                      color: isDarkTheme ? Colors.white : Colors.black,
                    ),
                  ),
                  SizedBox(
                    width: 1.w,
                  ),
                  reusableText(
                    text: "تحميل",
                    style: TextStyle(
                      fontFamily: 'Tajawal',
                      fontWeight: FontWeight.w400,
                      fontSize: 12,
                      color: isDarkTheme ? Colors.white : Colors.black,
                    ),
                  ),
                ],
              ),
            ],
          ),
          const Spacer(flex: 6),
          reusableMaterialButton(
            isDarkTheme: isDarkTheme,
            function: () => EasyLauncher.url(
              url: app.apkUrl,
            ),
            width: 20.w,
            height: 5.h,
            text: "تثبيت",
            borderColor:
                (Theme.of(context).buttonTheme.shape as RoundedRectangleBorder)
                    .side
                    .color,
          ),
        ],
      ),
    );
  }
}

class BuildAppShimmerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 8.h,
      margin: EdgeInsets.symmetric(vertical: 1.h),
      decoration: BoxDecoration(
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(2.w),
      ),
      child: Row(
        children: [
          Shimmer(
            child: Container(
              width: 15.w,
              height: 8.h,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(2.w),
                color: Colors.grey.shade300,
              ),
            ),
          ),
          const Spacer(flex: 1),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Shimmer(
                child: Container(
                  width: 20.w,
                  height: 2.h,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(2.w),
                    color: Colors.grey.shade300,
                  ),
                ),
              ),
              Shimmer(
                child: Container(
                  width: 30.w,
                  height: 2.h,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(2.w),
                    color: Colors.grey.shade300,
                  ),
                ),
              ),
              Shimmer(
                child: Container(
                  width: 15.w,
                  height: 2.h,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(2.w),
                    color: Colors.grey.shade300,
                  ),
                ),
              ),
            ],
          ),
          const Spacer(flex: 6),
          Shimmer(
            child: Container(
              width: 20.w,
              height: 5.h,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(2.w),
                color: Colors.grey.shade300,
                // border: Border.all(
                //   color: Colors.grey.shade300,
                // ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
