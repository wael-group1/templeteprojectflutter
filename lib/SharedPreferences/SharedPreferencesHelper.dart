import 'SharedPreferencesProvider.dart';

class AppSharedPreferences {
  static SharedPreferencesProvider? sharedPreferencesProvider;

  static init() async {
    sharedPreferencesProvider = await SharedPreferencesProvider.getInstance();
  }

  //lang
  static String get getLang =>
      sharedPreferencesProvider!.read('language') ?? "en";

  static saveLang(String value) =>
      sharedPreferencesProvider!.save('language', value);

  static bool get hasLang => sharedPreferencesProvider!.contains('language');

  static removeLang() => sharedPreferencesProvider!.remove('language');

  //token
  static String get getToken => sharedPreferencesProvider!.read('token') ?? '';

  static saveToken(String value) =>
      sharedPreferencesProvider!.save('token', value);

  static bool get hasToken => sharedPreferencesProvider!.contains('token');

  static removeToken() => sharedPreferencesProvider!.remove('token');

  static saveUdid(String value) =>
      sharedPreferencesProvider!.save('udid', value);

  static String get getUdid => sharedPreferencesProvider!.read('udid') ?? '';

  static saveExpiryDate(String date) =>
      sharedPreferencesProvider!.save('expiry_date', date);

  static get getExpiryDate => sharedPreferencesProvider!.read('expiry_date');
}
