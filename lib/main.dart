import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:servizio/App/Home/cubit/banner_cubit.dart';
import 'package:servizio/App/Settings/cubit/settings_cubit.dart';
import 'package:servizio/App/Settings/setting.dart';
import 'package:servizio/App/notification/cubit/notification_cubit.dart';
import 'package:servizio/App/notification/viewNotification.dart';
import 'package:sizer/sizer.dart';

import 'Apis/Network.dart';
import 'App/Auth/loginScreen.dart';
import 'App/Home/cubit/home_cubit.dart';
import 'App/Home/homeScreen.dart';
import 'App/Paint/Cubit/cubit.dart';
import 'App/Paint/Cubit/states.dart';
import 'constant/them.dart';
import 'observer/BlocObservable.dart';
import 'SharedPreferences/SharedPreferencesHelper.dart';
import 'utils/remote/Cach_Helper.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await CachHelper.init();
  await Network.init();
  await AppSharedPreferences.init();
  Bloc.observer = BlocObservable();
  bool? isDark = CachHelper.getData(key: 'isDark');
  runApp(MyApp(isDark: isDark));
}

class MyApp extends StatelessWidget {
  final bool? isDark;

  MyApp({this.isDark});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (_) => ThemeCubit()..changemode(fromShared: isDark),
          ),
          BlocProvider(create: (_) => HomeCubit()..initHome()),
          BlocProvider(
            create: (_) => NotificationsCubit(),
            child: const ViewNotificationScreen(),
          ),
          BlocProvider(
            create: (_) => BannerCubit()..getBanners(),
            child: HomeScreen(),
          ),
          BlocProvider(
            create: (_) => SettingsCubit(),
            child: const SettingsScreen(),
          ),
        ],
        child: BlocConsumer<ThemeCubit, ThemeStates>(
          listener: (context, state) {},
          builder: (context, state) {
            var themeCubit = BlocProvider.of<ThemeCubit>(context, listen: true);
            return Sizer(builder: (context, orientation, deviceType) {
              return MaterialApp(
                theme: lightThem,
                darkTheme: darkTheme,
                themeMode: themeCubit.isDark ? ThemeMode.dark : ThemeMode.light,
                locale: const Locale('ar'),
                supportedLocales: const [
                  Locale('ar'),
                  Locale('en'),
                ],
                localizationsDelegates: const [
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                  GlobalCupertinoLocalizations.delegate,
                ],
                debugShowCheckedModeBanner: false,
                home: AppSharedPreferences.getToken.isNotEmpty &&
                        AppSharedPreferences.getToken != null
                    ? HomeScreen()
                    : LoginScreen(),
                // routes: routs
              );
            });
          },
        ));
  }
}
