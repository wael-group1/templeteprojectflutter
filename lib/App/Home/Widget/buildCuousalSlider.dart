// import 'dart:async';
//
// import 'package:flutter/material.dart';
// import 'package:sizer/sizer.dart';
// import 'package:smooth_page_indicator/smooth_page_indicator.dart';
//
// import '../../../constant/appColor.dart';
// import '../../../constant/constimage.dart';
//
// class BuildCrousalSlider extends StatefulWidget {
//   @override
//   State<BuildCrousalSlider> createState() => _BuildCrousalSliderState();
// }
//
// class _BuildCrousalSliderState extends State<BuildCrousalSlider> {
//   final _pageController = PageController(initialPage: 0);
//   int _currentPage = 0;
//   Timer? _timer;
//
//   @override
//   void initState() {
//     super.initState();
//     _startAutoScroll();
//   }
//
//   void _startAutoScroll() {
//     Duration animationDuration;
//     _timer = Timer.periodic(
//       const Duration(seconds: 3),
//       (timer) {
//         if (_currentPage < 4) {
//           _currentPage++;
//           animationDuration = const Duration(milliseconds: 1000);
//         } else {
//           _currentPage = 0;
//           animationDuration = const Duration(milliseconds: 2000);
//         }
//         _pageController.animateToPage(
//           _currentPage,
//           duration: animationDuration,
//           curve: Curves.easeInOut,
//         );
//       },
//     );
//   }
//
//   @override
//   void dispose() {
//     _timer?.cancel();
//     _pageController.dispose();
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       children: [
//         SizedBox(
//           height: 20.h,
//           child: PageView.builder(
//             controller: _pageController,
//             onPageChanged: (index) => setState(() => _currentPage = index),
//             itemBuilder: (context, index) => Container(
//               width: double.infinity,
//               decoration: BoxDecoration(
//                 borderRadius: BorderRadius.circular(8.sp),
//               ),
//               child: Image.asset(
//                 index == 0
//                     ? AppImageAsset.crousal
//                     : index == 1
//                         ? AppImageAsset.appDetails1
//                         : index == 2
//                             ? AppImageAsset.appDetails2
//                             : index == 3
//                                 ? AppImageAsset.appDetails3
//                                 : AppImageAsset.appDetails4,
//                 fit: BoxFit.cover,
//               ),
//             ),
//             itemCount: 5,
//           ),
//         ),
//         SizedBox(height: 2.h),
//         SmoothPageIndicator(
//           controller: _pageController,
//           count: 5,
//           effect: const ExpandingDotsEffect(
//             activeDotColor: AppColor.primaryColor,
//             dotColor: AppColor.grey,
//             dotHeight: 8,
//             dotWidth: 8,
//           ),
//         ),
//       ],
//     );
//   }
// }
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:servizio/App/Home/cubit/banner_cubit.dart';
import 'package:servizio/App/Home/cubit/banner_state.dart';
import 'package:servizio/constant/appColor.dart';
import 'package:servizio/widget/try_again.dart';
import 'package:shimmer_animation/shimmer_animation.dart';
import 'package:sizer/sizer.dart';

class BuildCrousalSlider extends StatefulWidget {
  const BuildCrousalSlider({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _MyWidgetState createState() => _MyWidgetState();
}

class _MyWidgetState extends State<BuildCrousalSlider> {
  int currentIndex = 0;
  final CarouselController carouselController = CarouselController();

  @override
  Widget build(BuildContext context) {
    // var cubit = AboutAppCubit.get(context);
    return BlocBuilder<BannerCubit, BannerState>(
      builder: (context, state) {
        if (state is BannerLoadingState) {
          return LoadingCarouselSlider();
        }
        if (state is BannerSuccessState) {
          final banners = state.banners;
          return Column(
            children: [
              Container(
                clipBehavior: Clip.hardEdge,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: CarouselSlider(
                  items: List.generate(
                    banners.length,
                    (index) => CachedNetworkImage(
                      imageUrl: banners[index].bannerPath,
                      fit: BoxFit.cover,
                      width: double.infinity,
                    ),
                  ),
                  disableGesture: !(banners.length == 1),
                  carouselController: carouselController,
                  options: CarouselOptions(
                    scrollPhysics: const BouncingScrollPhysics(),
                    autoPlay: banners.length > 1,
                    aspectRatio: 2,
                    viewportFraction: 1,
                    onPageChanged: (index, reason) {
                      setState(
                        () {
                          currentIndex = index;
                        },
                      );
                    },
                  ),
                ),
              ),
              Center(
                child: DotsIndicator(
                  dotsCount: banners.length,
                  position: currentIndex,
                  onTap: (index) {
                    carouselController.animateToPage(index);
                  },
                  decorator: const DotsDecorator(
                    color: Colors.grey,
                    // Inactive dot color
                    activeColor: AppColor.primaryColor,
                    // Active dot color
                    size: Size.square(10.0),
                    activeSize: Size(20.0, 10.0),
                    spacing: EdgeInsets.all(5.0),
                  ),
                ),
              ),
            ],
          );
        } else {
          return TryAgainUi(
            () => context.read<BannerCubit>().getBanners(),
          );
        }
      },
    );
  }
}

class LoadingCarouselSlider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Shimmer(
          child: Container(
            width: double.infinity,
            height: 20.h,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(2.w),
                color: Colors.grey.shade300),
          ),
        ),
        SizedBox(height: 2.h),
        Shimmer(
          child: Container(
            width: 20.w,
            height: 2.h,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(2.w),
                color: Colors.grey.shade300),
          ),
        )
      ],
    );
  }
}
