// ignore_for_file: prefer_const_constructors, must_be_immutable
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:servizio/App/Applications/Pages/view_all_screen.dart';
import 'package:servizio/App/Auth/loginScreen.dart';
import 'package:servizio/App/Home/cubit/home_cubit.dart';
import 'package:servizio/App/Paint/Cubit/cubit.dart';
import 'package:servizio/App/Paint/paint.dart';
import 'package:servizio/App/Settings/setting.dart';
import 'package:servizio/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:servizio/componant.dart';
import 'package:servizio/constant/appColor.dart';
import 'package:servizio/constant/constimage.dart';
import 'package:servizio/utils/global_functions.dart';
import 'package:sizer/sizer.dart';

class MyDrawer extends StatelessWidget {
  MyDrawer({required this.scaffoldKey, super.key});

  GlobalKey<ScaffoldState> scaffoldKey;

  @override
  Widget build(BuildContext context) {
    var isDarkTheme = BlocProvider.of<ThemeCubit>(context).isDark;

    return Drawer(
      child: SafeArea(
        child: Scaffold(
          body: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Container(),
                    reusableText(
                      maxLines: 2,
                      text: 'متجر إبداع',
                      style: TextStyle(
                        fontFamily: 'Tajawal',
                        fontWeight: FontWeight.w700,
                        fontSize: 20,
                        color: isDarkTheme ? Colors.white : Colors.black,
                      ),
                    ),
                    IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Icon(
                        Icons.close,
                        size: 8.w,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 4.h,
                ),
                _DrawerItem(
                  'جميع التطبيقات',
                  Icons.more_outlined,
                  () => General.routingPage(
                    context,
                    ViewAllScreen(
                      title: 'جميع التطبيقات',
                      apps: context.read<HomeCubit>().apps,
                    ),
                  ),
                ),
                SizedBox(
                  height: 5.h,
                ),
                _DrawerItem(
                  "التطبيقات المحملة",
                  Icons.file_download_outlined,
                  () {},
                ),
                SizedBox(
                  height: 5.h,
                ),
                _DrawerItem(
                  "شارك التطبيق",
                  Icons.share_outlined,
                  () {},
                ),
                SizedBox(
                  height: 5.h,
                ),
                _DrawerItem(
                  "الأعدادت",
                  Icons.settings_outlined,
                  () => General.routingPage(context, SettingsScreen()),
                ),
                SizedBox(
                  height: 5.h,
                ),
                _DrawerItem(
                  "تلوين التطبيق",
                  Icons.format_paint_outlined,
                  () => General.routingPage(context, PaintScreen()),
                ),
                Spacer(),
                Center(
                  child: Container(
                    width: 75.w,
                    height: 8.h,
                    decoration: BoxDecoration(
                      color: AppColor.primaryColor,
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: MaterialButton(
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        side: BorderSide(
                          color: AppColor.primaryColor,
                        ),
                      ),
                      onPressed: () {
                        AppSharedPreferences.removeToken();
                        General.removeUntilRoutingPage(
                          context,
                          LoginScreen(),
                        );
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.logout,
                            color: Colors.white,
                          ),
                          SizedBox(width: 4.w),
                          reusableText(
                            text: "تسجيل الخروج",
                            style: TextStyle(
                              fontFamily: 'Tajawal',
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _DrawerItem extends StatelessWidget {
  final String title;
  final IconData icon;
  final void Function()? onPressed;

  const _DrawerItem(this.title, this.icon, this.onPressed);

  @override
  Widget build(BuildContext context) {
    var isDarkTheme = BlocProvider.of<ThemeCubit>(context).isDark;

    return InkWell(
      onTap: onPressed,
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 2.w,
          vertical: 1.h,
        ),
        child: Row(
          children: [
            Icon(icon),
            SizedBox(
              width: 2.w,
            ),
            reusableText(
              text: title,
              style: TextStyle(
                fontFamily: 'Tajawal',
                fontWeight: FontWeight.w400,
                fontSize: 18,
                color: isDarkTheme ? Colors.white : Colors.black,
              ),
            )
          ],
        ),
      ),
    );
  }
}
