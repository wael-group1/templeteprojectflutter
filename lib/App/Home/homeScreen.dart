import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:servizio/App/Applications/Pages/view_all_screen.dart';
import 'package:servizio/App/Home/Widget/buildDrawer.dart';
import 'package:servizio/App/Home/cubit/home_cubit.dart';
import 'package:servizio/App/Home/cubit/home_state.dart';
import 'package:servizio/App/notification/viewNotification.dart';
import 'package:servizio/utils/global_functions.dart';
import 'package:servizio/App/Home/Widget/buildCuousalSlider.dart';
import 'package:servizio/componant.dart';
import 'package:servizio/constant/appColor.dart';
import 'package:servizio/widget/buildApplicationWidget.dart';
import 'package:servizio/widget/try_again.dart';
import 'package:shimmer_animation/shimmer_animation.dart';
import 'package:sizer/sizer.dart';
import '../Paint/Cubit/cubit.dart';
import 'models/app_model.dart';
import 'models/sections_response.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final globalKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    var searchController = TextEditingController();
    var isDarkTheme = BlocProvider.of<ThemeCubit>(context).isDark;

    return SafeArea(
      child: Scaffold(
        key: globalKey,
        drawer: MyDrawer(
          scaffoldKey: globalKey,
        ),
        appBar: AppBar(
          backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
          leading: IconButton(
            onPressed: () {
              FocusScope.of(context).unfocus();
              globalKey.currentState!.openDrawer();
            },
            icon: Icon(
              Icons.menu,
              size: 10.w,
            ),
            color: AppColor.primaryColor,
          ),
          title: reusableText(
            text: 'متجر إبداع',
            style: TextStyle(
              fontFamily: 'Tajawal',
              fontWeight: FontWeight.w700,
              fontSize: 18,
              color: isDarkTheme ? Colors.white : Colors.black,
            ),
          ),
          centerTitle: true,
          actions: [
            IconButton(
              onPressed: () {
                General.routingPage(
                  context,
                  const ViewNotificationScreen(),
                );
              },
              icon: Icon(
                Icons.notifications_none,
                size: 10.w,
              ),
              color: AppColor.primaryColor,
            ),
          ],
        ),
        body: BlocBuilder<HomeCubit, HomeState>(
          builder: (context, state) {
            if (state is SectionsLoadingState) {
              return Padding(
                padding: EdgeInsets.only(left: 4.w, right: 4.w, top: 2.h),
                child: DefaultTabController(
                  length: 5,
                  child: Column(
                    children: [
                      reusableTextForm(
                        context: context,
                        controller: searchController,
                        keyboardType: TextInputType.text,
                        isDarkTheme: isDarkTheme,
                        labelText: "بحث",
                        iconSuffix: Icons.search,
                        hintText: "بحث",
                        radius: 8.w,
                        validate: (value) {
                          if (value!.isEmpty) {
                            return "يجب الا يكون حقل البحث فارغا";
                          }
                          return null;
                        },
                      ),
                      _LoadingTabBar(),
                      _LoadingTabBarView(),
                    ],
                  ),
                ),
              );
            }
            if (state is SectionsSuccessState) {
              final sections = context.read<HomeCubit>().sections;
              final apps = context.read<HomeCubit>().apps;
              final filteredApps = context.read<HomeCubit>().filteredApps;
              return Padding(
                padding: EdgeInsets.only(left: 4.w, right: 4.w, top: 2.h),
                child: DefaultTabController(
                  length: sections.length,
                  child: Column(
                    children: [
                      reusableTextForm(
                        context: context,
                        controller: searchController,
                        keyboardType: TextInputType.text,
                        isDarkTheme: isDarkTheme,
                        labelText: "بحث",
                        iconSuffix: Icons.search,
                        hintText: "بحث",
                        radius: 8.w,
                        validate: (value) {
                          if (value!.isEmpty) {
                            return "يجب الا يكون حقل البحث فارغا";
                          }
                          return null;
                        },
                      ),
                      _TabBar(sections: sections, apps: apps),
                      _TabBarView(
                        sections: sections,
                        apps: apps,
                        filteredApps: filteredApps,
                      ),
                    ],
                  ),
                ),
              );
            } else {
              return TryAgainUi(() => context.read<HomeCubit>().initHome());
            }
          },
        ),
      ),
    );
  }
}

class _TabBar extends StatelessWidget {
  final List<Section> sections;
  final List<App> apps;

  const _TabBar({required this.sections, required this.apps});

  @override
  Widget build(BuildContext context) {
    var isDarkTheme = BlocProvider.of<ThemeCubit>(context).isDark;

    return TabBar(
      tabAlignment: TabAlignment.center,
      indicatorSize: TabBarIndicatorSize.tab,
      indicatorColor: AppColor.primaryColor,
      isScrollable: true,
      tabs: sections
          .map(
            (section) => Tab(
              child: reusableText(
                text: section.sectionName,
                style: TextStyle(
                  fontFamily: 'Tajawal',
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                  color: isDarkTheme ? Colors.white : Colors.black,
                ),
              ),
            ),
          )
          .toList(),
    );
  }
}

class _LoadingTabBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TabBar(
      tabs: List.generate(
        5,
        (index) => Tab(
          child: Shimmer(
            child: Container(
              height: 2.h,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(2.w),
                color: Colors.grey.shade300,
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _TabBarView extends StatelessWidget {
  final List<Section> sections;
  final List<App> apps;
  final Map<String, dynamic> filteredApps;

  const _TabBarView({
    required this.sections,
    required this.apps,
    required this.filteredApps,
  });

  @override
  Widget build(BuildContext context) {
    var isDarkTheme = BlocProvider.of<ThemeCubit>(context).isDark;

    return Expanded(
      child: TabBarView(
        children: sections
            .map(
              (section) => section.sectionId == '0'
                  ? _HomeTapBar(sections: sections)
                  : filteredApps[section.sectionId] != null
                      ? ListView.builder(
                          itemCount: filteredApps[section.sectionId]!.length,
                          itemBuilder: (context, index) => BuildAppWidget(
                            app: filteredApps[section.sectionId]![index],
                          ),
                        )
                      : Center(
                          child: Text(
                            'لا يوجد تطبيقات لعرضها',
                            style: TextStyle(
                              fontFamily: 'Tajawal',
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                              color: isDarkTheme ? Colors.white : Colors.black,
                            ),
                          ),
                        ),
            )
            .toList(),
      ),
    );
  }
}

class _HomeTapBar extends StatelessWidget {
  final List<Section> sections;

  _HomeTapBar({required this.sections});

  @override
  Widget build(BuildContext context) {
    final filteredApps = context.read<HomeCubit>().filteredApps;
    final isDarkTheme = BlocProvider.of<ThemeCubit>(context).isDark;
    return ListView(
      children: [
        SizedBox(height: 1.h),
        const BuildCrousalSlider(),
        SizedBox(height: 1.h),
        ...sections.map(
          (section) => filteredApps[section.sectionId] == null
              ? Container()
              : Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        reusableText(
                          text: section.sectionName,
                          style: TextStyle(
                            fontFamily: 'Tajawal',
                            fontWeight: FontWeight.w700,
                            fontSize: 14,
                            color: isDarkTheme ? Colors.white : Colors.black,
                          ),
                        ),
                        IconButton(
                          onPressed: () {
                            General.routingPage(
                              context,
                              ViewAllScreen(
                                title: section.sectionName,
                                apps: filteredApps[section.sectionId]!,
                              ),
                            );
                          },
                          icon: Icon(
                            Icons.arrow_forward,
                            color: isDarkTheme
                                ? Colors.white
                                : AppColor.primaryColor,
                          ),
                        )
                      ],
                    ),
                    ...List.generate(
                      min(2, filteredApps[section.sectionId]!.length),
                      (index) => BuildAppWidget(
                        app: filteredApps[section.sectionId]![index],
                      ),
                    )
                  ],
                ),
        ),
      ],
    );
  }
}

class _LoadingTabBarView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: TabBarView(
        children: List.generate(
          5,
          (index) => index == 0
              ? _LoadingHomeTabBar()
              : ListView.builder(
                  itemCount: 15,
                  itemBuilder: (context, index) => BuildAppShimmerWidget(),
                ),
        ),
      ),
    );
  }
}

class _LoadingHomeTabBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        SizedBox(height: 1.h),
        LoadingCarouselSlider(),
        SizedBox(height: 1.h),
        ...List.generate(
          5,
          (index) => Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 30.w,
                    height: 2.h,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(2.w),
                      color: Colors.grey.shade300,
                    ),
                  ),
                  Container(
                    width: 8.w,
                    height: 8.w,
                    decoration: ShapeDecoration(
                      shape: const CircleBorder(),
                      color: Colors.grey.shade300,
                    ),
                  )
                ],
              ),
              ...List.generate(
                2,
                (index) => BuildAppShimmerWidget(),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
