import 'package:flutter/material.dart';

@immutable
abstract class HomeState {}

class HomeInitialState extends HomeState {}

class SectionsLoadingState extends HomeState {}

class SectionsSuccessState extends HomeState {
  SectionsSuccessState();
}

class SectionsErrorState extends HomeState {
  final String message;

  SectionsErrorState(this.message);
}

class AppsLoadingState extends HomeState {}

class AppsSuccessState extends HomeState {
  AppsSuccessState();
}

class AppsErrorState extends HomeState {
  final String message;

  AppsErrorState(this.message);
}
