import 'package:bloc/bloc.dart';

// State
import 'banner_state.dart';

// Model
import '../models/banner_response.dart' as banner_model;

// Network
import '../../../Apis/Network.dart';
import '../../../constant/urls.dart';

class BannerCubit extends Cubit<BannerState> {
  BannerCubit() : super(BannerInitState());

  getBanners() async {
    emit(BannerLoadingState());
    try {
      final response = await Network.getData(url: Urls.banners);
      final List<banner_model.Banner> bannersList = [];
      for (var banner in (response.data as List<dynamic>)) {
        bannersList.add(banner_model.Banner.fromJson(banner));
      }
      emit(BannerSuccessState(bannersList));
    } catch (e) {
      emit(BannerErrorState());
    }
  }
}
