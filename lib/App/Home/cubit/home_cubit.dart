import 'package:bloc/bloc.dart';
import 'package:servizio/SharedPreferences/SharedPreferencesHelper.dart';

// State
import 'home_state.dart';

// Model
import '../models/sections_response.dart';
import '../models/app_model.dart';

// Network
import '../../../Apis/Network.dart';
import '../../../constant/urls.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(HomeInitialState());
  late final List<Section> sections;
  late final List<App> apps;
  Map<String, List<App>> filteredApps = {};

  void initHome() async {
    emit(SectionsLoadingState());
    try {
      await Future.wait([getSectionsData(), getAppsData()]);
      filteredApps['0'] = apps;
      for (var app in apps) {
        if (filteredApps.containsKey(app.sectionsId)) {
          filteredApps[app.sectionsId]!.add(app);
        } else {
          filteredApps[app.sectionsId] = [app];
        }
      }
      emit(SectionsSuccessState());
    } catch (e) {
      emit(SectionsErrorState(e.toString()));
    }
  }

  void getSections() async {
    emit(SectionsLoadingState());
    try {
      await getSectionsData();
      emit(SectionsSuccessState());
    } catch (e) {
      emit(SectionsErrorState(e.toString()));
    }
  }

  Future<void> getSectionsData() async {
    try {
      final response = await Network.postData(
        url: Urls.getSections,
        data: {
          "key": AppSharedPreferences.getToken,
          "udid": AppSharedPreferences.getUdid,
          "token": "3Z-ALL998128-HOPIJkL-IO9779"
        },
      );
      sections = sectionsResponseFromJson(response.data).sections;
    } catch (e) {
      rethrow;
    }
  }

  Future<void> getApps() async {
    emit(AppsLoadingState());
    try {
      await getAppsData();

      emit(AppsSuccessState());
    } catch (e) {
      emit(AppsErrorState(e.toString()));
    }
  }

  Future<void> getAppsData() async {
    try {
      final response = await Network.postData(
        url: Urls.getApps,
        data: {
          "key": "4JICAHRIM1ZK520282ZS",
          "udid": "435bdb254ef545f2",
          "token": "3Z-ALL998128-HOPIJkL-IO9779"
        },
      );
      apps = appFromJson(response.data);
    } catch (e) {
      rethrow;
    }
  }
}
