import 'package:flutter/material.dart';
import 'package:equatable/equatable.dart';

// Model
import '../models/banner_response.dart' as response;

@immutable
abstract class BannerState extends Equatable {}

class BannerInitState extends BannerState {
  @override
  List<Object?> get props => [];
}

class BannerLoadingState extends BannerState {
  @override
  List<Object?> get props => [];
}

class BannerSuccessState extends BannerState {
  final List<response.Banner> banners;

  BannerSuccessState(this.banners);

  @override
  List<Object?> get props => [banners];
}

class BannerErrorState extends BannerState {
  @override
  List<Object?> get props => [];
}
