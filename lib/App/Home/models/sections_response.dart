import 'dart:convert';

import 'app_model.dart';

SectionsResponse sectionsResponseFromJson(String str) => SectionsResponse.fromJson(json.decode(str));

String sectionsResponseToJson(SectionsResponse data) => json.encode(data.toJson());

class SectionsResponse {
  final List<Section> sections;

  SectionsResponse({
    required this.sections,
  });

  factory SectionsResponse.fromJson(Map<String, dynamic> json) => SectionsResponse(
    sections: List<Section>.from(json["Sections"].map((x) => Section.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "Sections": List<dynamic>.from(sections.map((x) => x.toJson())),
  };
}

class Section {
  final String sectionId;
  final DateTime sectionCreateKey;
  final String sectionName;
  List<App> apps;

  Section({
    required this.sectionId,
    required this.sectionCreateKey,
    required this.sectionName,
    this.apps = const [],
  });

  factory Section.fromJson(Map<String, dynamic> json) => Section(
    sectionId: json["Sections_Id"],
    sectionCreateKey: DateTime.parse(json["Sections_CreateKey"]),
    sectionName: json["Sections_Name"],
  );

  Map<String, dynamic> toJson() => {
    "Sections_Id": sectionId,
    "Sections_CreateKey": "${sectionCreateKey.year.toString().padLeft(4, '0')}-${sectionCreateKey.month.toString().padLeft(2, '0')}-${sectionCreateKey.day.toString().padLeft(2, '0')}",
    "Sections_Name": sectionName,
  };
}
