import 'dart:convert';

List<Banner> bannerResponseFromJson(String str) =>
    List<Banner>.from(
        json.decode(str).map((x) => Banner.fromJson(x)));

String bannerResponseToJson(List<Banner> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Banner {
  final String id;
  final String bannerPath;
  final String bannerUrl;

  Banner({
    required this.id,
    required this.bannerPath,
    required this.bannerUrl,
  });

  factory Banner.fromJson(Map<String, dynamic> json) => Banner(
        id: json["id"],
        bannerPath: json["banner_path"],
        bannerUrl: json["banner_url"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "banner_path": bannerPath,
        "banner_url": bannerUrl,
      };
}
