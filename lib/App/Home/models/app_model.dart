import 'dart:convert';

List<App> appFromJson(String str) => List<App>.from(json.decode(str).map((x) => App.fromJson(x)));

String appToJson(List<App> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class App {
  final String id;
  final String icon;
  final String section;
  final String name;
  final String version;
  final String bundled;
  final String date;
  final String apkUrl;
  final String sectionsId;
  final String sizeFile;
  final String description;
  final String views;

  App({
    required this.id,
    required this.icon,
    required this.section,
    required this.name,
    required this.version,
    required this.bundled,
    required this.date,
    required this.apkUrl,
    required this.sectionsId,
    required this.sizeFile,
    required this.description,
    required this.views,
  });

  factory App.fromJson(Map<String, dynamic> json) => App(
    id: json["id"],
    icon: json["Icon"],
    section: json["Section"],
    name: json["Name"],
    version: json["Version"],
    bundled: json["Bundleid"],
    date: json["Data"],
    apkUrl: json["NSURL"],
    sectionsId: json["Sections_Id"],
    sizeFile: json["SizeFile"],
    description: json["Description"],
    views: json["views"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "Icon": icon,
    "Section": section,
    "Name": name,
    "Version": version,
    "Bundleid": bundled,
    "Data": date,
    "NSURL": apkUrl,
    "Sections_Id": sectionsId,
    "SizeFile": sizeFile,
    "Description": description,
    "views": views,
  };
}
