import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:servizio/SharedPreferences/SharedPreferencesHelper.dart';

// State
import 'auth_state.dart';

// Network
import '../../../Apis/Network.dart';
import '../../../constant/urls.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit() : super(AuthInitState());

  login(String key, String udid) async {
    emit(AuthLoadingState());
    try {
      final response = await Network.postData(
        url: Urls.login,
        data: {
          "key": key,
          "udid": udid,
          "token": "3Z-ALL998128-HOPIJkL-IO9779"
        },
      );
      final responseData = jsonDecode(response.data) as Map<String, dynamic>;
      if (responseData['Checked_0x'] == 'Yes') {
        AppSharedPreferences.saveExpiryDate(responseData['Finished_Time']);
        AppSharedPreferences.saveToken(key);
        AppSharedPreferences.saveUdid(udid);
        emit(AuthSuccessState());
      } else {
        throw Exception();
      }
    } catch (e) {
      emit(AuthErrorState(e.toString()));
    }
  }
}
