import 'package:easy_url_launcher/easy_url_launcher.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:servizio/App/Auth/cubit/auth_cubit.dart';
import 'package:servizio/App/Home/homeScreen.dart';
import 'package:servizio/componant.dart';
import 'package:servizio/constant/appColor.dart';
import 'package:servizio/utils/global_functions.dart';
import 'package:servizio/widget/loading_widgets/loading_dialog.dart';
import 'package:servizio/widget/snack_bar.dart';
import 'package:sizer/sizer.dart';
import 'package:flutter_udid/flutter_udid.dart';

import '../Paint/Cubit/cubit.dart';
import 'cubit/auth_state.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final AuthCubit _authCubit = AuthCubit();
  var codeController = TextEditingController();
  String? errorMessage;
  String _udid = 'Unknown';

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  Future<void> initPlatformState() async {
    String udid;
    try {
      udid = await FlutterUdid.udid;
    } on PlatformException {
      udid = 'Failed to get UDID.';
    }

    if (!mounted) return;

    setState(
      () {
        _udid = udid;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var isDarkTheme = BlocProvider.of<ThemeCubit>(context).isDark;

    return SafeArea(
      child: Scaffold(
        body: BlocProvider(
          create: (context) => _authCubit,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 10.h),
                  reusableText(
                    text: 'مرحباً بك\nقم بتسجيل الدخول للاستمرار',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Tajawal',
                      fontWeight: FontWeight.w700,
                      fontSize: 18,
                      color: isDarkTheme ? Colors.white : Colors.black,
                    ),
                    maxLines: 2,
                  ),
                  SizedBox(height: 5.h),
                  Form(
                    key: _formKey,
                    child: reusableTextForm(
                      context: context,
                      controller: codeController,
                      isDarkTheme: isDarkTheme,
                      keyboardType: TextInputType.text,
                      labelText: "ادخل الكود",
                      hintText: "ادخل الكود",
                      radius: 8.w,
                      validate: (value) {
                        if (value!.isEmpty) {
                          setState(
                              () => errorMessage = "يجب الا يكون الكود فارغا");
                          return errorMessage;
                        }
                        return null;
                      },
                    ),
                  ),
                  BlocListener<AuthCubit, AuthState>(
                    listener: (context, state) {
                      if (state is AuthLoadingState) {
                        showLoadingDialog(context);
                      }
                      if (state is AuthSuccessState) {
                        Navigator.pop(context);
                        General.removeUntilRoutingPage(context, HomeScreen());
                      }
                      if (state is AuthErrorState) {
                        Navigator.pop(context);
                        showSnackBar(
                          'هذا الكود خاطئ يرجى ادخال كود صحيح',
                          context,
                        );
                      }
                    },
                    child: Padding(
                      padding: EdgeInsets.only(top: 10.h),
                      child: ElevatedButton(
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            _authCubit.login(codeController.text, _udid);
                          }
                        },
                        style: ElevatedButton.styleFrom().copyWith(
                          backgroundColor: const WidgetStatePropertyAll(
                            AppColor.primaryColor,
                          ),
                          shape: WidgetStatePropertyAll(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          // fixedSize: WidgetStatePropertyAll(
                          //   Size(40.w, 7.h),
                          // ),
                        ),
                        child: const Text(
                          'تسجيل الدخول',
                          style: TextStyle(
                            fontFamily: 'Tajawal',
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 25.h),
                  reusableText(
                    text: "هل تحتاج الى كود؟",
                    style: TextStyle(
                      fontFamily: 'Tajawal',
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                      color: isDarkTheme ? Colors.white : Colors.black,
                    ),
                  ),
                  SizedBox(height: 5.h),
                  ElevatedButton(
                    onPressed: () => EasyLauncher.url(
                      url: 'http://ibda3-android.com',
                    ),
                    style: ElevatedButton.styleFrom().copyWith(
                      backgroundColor: const WidgetStatePropertyAll(
                        AppColor.primaryColor,
                      ),
                      shape: WidgetStatePropertyAll(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      // fixedSize: WidgetStatePropertyAll(
                      //   Size(80.w, 7.h),
                      // ),
                    ),
                    child: const Text(
                      'شراء كود',
                      style: TextStyle(
                        fontFamily: 'Tajawal',
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
