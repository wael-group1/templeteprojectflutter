
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:servizio/App/Paint/Cubit/states.dart';
import 'package:servizio/utils/remote/Cach_Helper.dart';

class ThemeCubit extends Cubit<ThemeStates>
{
  ThemeCubit ():super (Themeinitialstate());
  static ThemeCubit get(context)=>BlocProvider.of(context);
  bool isDark =false;
 
  void changemode({  bool? fromShared}) 
  {
    if(fromShared!=null)
      isDark=fromShared;
    else
    isDark=!isDark;
    CachHelper.putData(key: 'isDark', value: isDark).then((value)
        {
          emit(ThemeChangeModeState());
        }
    );
  }
}
//https://newsapi.org/v2/everything?q=tesla&apiKey=9513ab9e21c840c2978269b0a86ee9ab