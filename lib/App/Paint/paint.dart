import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:servizio/App/Paint/Cubit/cubit.dart';
import 'package:servizio/App/Paint/Cubit/states.dart';
import 'package:servizio/componant.dart';
import 'package:sizer/sizer.dart';

class PaintScreen extends StatelessWidget {
  const PaintScreen({super.key});

  @override
  Widget build(BuildContext context) {
    var themeCubit = BlocProvider.of<ThemeCubit>(context, listen: true);
    var isDarkTheme = BlocProvider.of<ThemeCubit>(context).isDark;

    return SafeArea(
      child: Scaffold(
        body: BlocConsumer<ThemeCubit, ThemeStates>(
          listener: (context, state) {
            // TODO: implement listener
          },
          builder: (context, state) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Spacer(),
                    reusableText(
                      maxLines: 2,
                      text: "تلوين التطبيق",
                      style: TextStyle(
                        fontFamily: 'Tajawal',
                        fontWeight: FontWeight.w700,
                        fontSize: 18,
                        color: isDarkTheme ? Colors.white : Colors.black,
                      ),
                    ),
                    const Spacer(),
                    IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: Icon(
                          Icons.arrow_forward,
                          size: 8.w,
                          color: Theme.of(context).iconTheme.color!,
                        )),
                  ],
                ),
                SizedBox(
                  height: 3.h,
                ),
                GestureDetector(
                  onTap: () {
                    themeCubit.changemode();
                  },
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10.w),
                    child: Container(
                      height: 30.h,
                      width: 50.w,
                      decoration: BoxDecoration(
                          color: Color(0xff585B5F),
                          borderRadius: BorderRadius.circular(20)),
                      child: Stack(
                        children: [
                          Column(
                            children: [
                              SizedBox(
                                height: 2.h,
                              ),
                              Center(
                                child: Container(
                                  height: 3.h,
                                  width: 40.w,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(20)),
                                ),
                              ),
                              SizedBox(
                                height: 2.h,
                              ),
                              Expanded(
                                child: GridView.builder(
                                  gridDelegate:
                                      SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 2,
                                          childAspectRatio: 2.5,
                                          crossAxisSpacing: 2.w,
                                          mainAxisSpacing: 1.h),
                                  itemBuilder: (context, index) => Container(
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                  ),
                                  itemCount: 8,
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 2.w),
                                  physics: const NeverScrollableScrollPhysics(),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }
}
