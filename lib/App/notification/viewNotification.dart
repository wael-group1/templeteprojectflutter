import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_udid/flutter_udid.dart';
import 'package:servizio/App/notification/Widget/buildNotification.dart';
import 'package:servizio/App/notification/cubit/notification_cubit.dart';
import 'package:servizio/App/notification/cubit/notification_state.dart';
import 'package:servizio/App/notification/models/notification_response.dart'
    as response;
import 'package:servizio/componant.dart';
import 'package:servizio/widget/try_again.dart';
import 'package:sizer/sizer.dart';

import '../Paint/Cubit/cubit.dart';

class ViewNotificationScreen extends StatefulWidget {
  const ViewNotificationScreen({super.key});

  @override
  State<ViewNotificationScreen> createState() => _ViewNotificationScreenState();
}

class _ViewNotificationScreenState extends State<ViewNotificationScreen> {

  @override
  void initState() {
    super.initState();
    context.read<NotificationsCubit>().getNotifications();
  }

  @override
  Widget build(BuildContext context) {
    var isDarkTheme = BlocProvider.of<ThemeCubit>(context).isDark;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            onPressed: () => Navigator.pop(context),
            icon: const Icon(Icons.arrow_forward),
          ),
        ],
        title: reusableText(
          text: "الإشعارات",
          style: TextStyle(
            fontFamily: 'Tajawal',
            fontWeight: FontWeight.w700,
            fontSize: 18,
            color: isDarkTheme ? Colors.white : Colors.black,
          ),
        ),
        centerTitle: true,
      ),
      body: BlocBuilder<NotificationsCubit, NotificationState>(
        builder: (context, state) {
          if (state is NotificationsLoadingState) {
            return Padding(
              padding: EdgeInsets.only(left: 4.w, right: 4.w, top: 2.h),
              child: Column(
                children: [
                  Expanded(
                    child: ListView.separated(
                      separatorBuilder: (context, index) => SizedBox(
                        height: 2.h,
                      ),
                      itemCount: 10,
                      itemBuilder: (context, index) {
                        return BuildLoadingNotification();
                      },
                      scrollDirection: Axis.vertical,
                    ),
                  ),
                ],
              ),
            );
          }
          if (state is NotificationsSuccessState) {
            final List<response.Notification> notifications =
                state.notifications.notifications;
            return notifications.isEmpty
                ? Center(
                    child: Text(
                      'لا يوجد اشعارات لعرضها',
                      style: TextStyle(
                        fontFamily: 'Tajawal',
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                        color: isDarkTheme ? Colors.white : Colors.black,
                      ),
                    ),
                  )
                : Padding(
                    padding: EdgeInsets.only(left: 4.w, right: 4.w, top: 2.h),
                    child: Column(
                      children: [
                        Expanded(
                          child: ListView.separated(
                            separatorBuilder: (context, index) => SizedBox(
                              height: 2.h,
                            ),
                            itemCount: notifications.length,
                            itemBuilder: (context, index) {
                              return BuildNotification(
                                notification: notifications[index],
                              );
                            },
                            scrollDirection: Axis.vertical,
                          ),
                        ),
                      ],
                    ),
                  );
          } else {
            return TryAgainUi(
              () => context.read<NotificationsCubit>().getNotifications(),
            );
          }
        },
      ),
    );
  }
}
