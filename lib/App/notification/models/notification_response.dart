import 'dart:convert';

NotificationsResponse notificationsResponseFromJson(String str) =>
    NotificationsResponse.fromJson(json.decode(str));

String notificationsResponseToJson(NotificationsResponse data) =>
    json.encode(data.toJson());

class NotificationsResponse {
  final List<Notification> notifications;

  NotificationsResponse({
    required this.notifications,
  });

  factory NotificationsResponse.fromJson(Map<String, dynamic> json) =>
      NotificationsResponse(
        notifications: json["Notifications"] == "null"
            ? []
            : List<Notification>.from(
          json["List_Notifications"].map(
                  (x) => Notification.fromJson(x),
                ),
        ),
      );

  Map<String, dynamic> toJson() => {
        "List_Notifications":
            List<dynamic>.from(notifications.map((x) => x.toJson())),
      };
}

class Notification {
  final String id;
  final String title;
  final String description;
  final DateTime createdAt;

  Notification({
    required this.id,
    required this.title,
    required this.description,
    required this.createdAt,
  });

  factory Notification.fromJson(Map<String, dynamic> json) => Notification(
        id: json["id"],
        title: json["Title"],
        description: json["Description"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "Title": title,
        "Description": description,
        "created_at": createdAt.toIso8601String(),
      };
}
