import 'package:flutter/material.dart';
import 'package:equatable/equatable.dart';

// Model
import '../models/notification_response.dart';

@immutable
abstract class NotificationState extends Equatable {}

class NotificationInitState extends NotificationState {
  @override
  List<Object?> get props => [];
}

class NotificationsLoadingState extends NotificationState {
  @override
  List<Object?> get props => [];
}

class NotificationsSuccessState extends NotificationState {
  final NotificationsResponse notifications;

  NotificationsSuccessState(this.notifications);

  @override
  List<Object?> get props => [notifications];
}

class NotificationErrorState extends NotificationState {
  @override
  List<Object?> get props => [];
}