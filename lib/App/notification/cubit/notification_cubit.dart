import 'package:bloc/bloc.dart';
import 'package:servizio/Apis/Network.dart';
import 'package:servizio/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:servizio/constant/urls.dart';

// State
import 'notification_state.dart';

// Model
import '../models/notification_response.dart';

class NotificationsCubit extends Cubit<NotificationState> {
  NotificationsCubit() : super(NotificationInitState());

  getNotifications() async {
    emit(NotificationsLoadingState());
    try {
      final response = await Network.postData(
        url: Urls.notifications,
        data: {
          "key": AppSharedPreferences.getToken,
          "udid": AppSharedPreferences.getUdid,
          "token": "3Z-ALL998128-HOPIJkL-IO9779"
        },
      );
      final notifications = notificationsResponseFromJson(response.data);
      emit(NotificationsSuccessState(notifications));
    } catch (e) {
      emit(NotificationErrorState());
    }
  }
}
