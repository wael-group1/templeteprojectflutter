import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shimmer_animation/shimmer_animation.dart';
import '../models/notification_response.dart' as response;
import 'package:servizio/componant.dart';
import 'package:servizio/constant/appColor.dart';
import 'package:sizer/sizer.dart';

import '../../Paint/Cubit/cubit.dart';

class BuildNotification extends StatelessWidget {
  final response.Notification notification;

  const BuildNotification({required this.notification, super.key});

  @override
  Widget build(BuildContext context) {
    var isDarkTheme = BlocProvider.of<ThemeCubit>(context).isDark;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 6.w),
      width: double.infinity,
      height: 15.h,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CircleAvatar(
            minRadius: 6.w,
            backgroundColor: AppColor.primaryColor,
            child: Icon(
              Icons.notifications_none,
              size: 8.w,
              color: Colors.white,
            ),
          ),
          SizedBox(width: 3.w),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                reusableText(
                  text: notification.title,
                  style: TextStyle(
                    fontFamily: 'Tajawal',
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    color: isDarkTheme ? Colors.white : Colors.black,
                  ),
                ),
                reusableText(
                  text: notification.description,
                  style: TextStyle(
                    fontFamily: 'Tajawal',
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    color: isDarkTheme ? Colors.white : Colors.black,
                  ),
                ),
                SizedBox(
                  height: 1.h,
                ),
                reusableText(
                  text: notification.createdAt.toString(),
                  style: const TextStyle(
                    fontFamily: 'Tajawal',
                    fontWeight: FontWeight.w500,
                    fontSize: 8,
                    color: AppColor.grey,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class BuildLoadingNotification extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 6.w),
      width: double.infinity,
      height: 15.h,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(2.w),
        border: Border.all(color: Colors.grey.shade300),
        color: Colors.transparent,
      ),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(6.w),
            child: Shimmer(
              child: Container(
                width: 12.w,
                height: 12.w,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.grey.shade300,
                ),
              ),
            ),
          ),
          SizedBox(width: 3.w),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Shimmer(
                  child: Container(
                    width: 20.w,
                    height: 2.h,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(2.w),
                      color: Colors.grey.shade300,
                    ),
                  ),
                ),
                SizedBox(height: 1.h),
                Shimmer(
                  child: Container(
                    width: 30.w,
                    height: 2.h,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(2.w),
                      color: Colors.grey.shade300,
                    ),
                  ),
                ),
                SizedBox(height: 1.h),
                Shimmer(
                  child: Container(
                    width: 15.w,
                    height: 2.h,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(2.w),
                      color: Colors.grey.shade300,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
