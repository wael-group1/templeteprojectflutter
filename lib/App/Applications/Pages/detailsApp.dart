import 'package:easy_url_launcher/easy_url_launcher.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:servizio/componant.dart';
import 'package:servizio/constant/appColor.dart';
import 'package:sizer/sizer.dart';
import 'package:flutter_rating/flutter_rating.dart';

import '../../Home/models/app_model.dart';
import '../../Paint/Cubit/cubit.dart';

class DetailsAppScreen extends StatefulWidget {
  const DetailsAppScreen({
    super.key,
    required this.app,
  });

  final App app;

  @override
  State<DetailsAppScreen> createState() => _DetailsAppScreenState();
}

class _DetailsAppScreenState extends State<DetailsAppScreen> {
  double rating = 3.5;
  int starCount = 5;
  bool isFavorite = false;

  @override
  Widget build(BuildContext context) {
    var isDarkTheme = BlocProvider.of<ThemeCubit>(context).isDark;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: const Icon(Icons.arrow_forward),
          ),
        ],
        title: reusableText(
          text: "متجر إبداع",
          style: TextStyle(
            fontFamily: 'Tajawal',
            fontWeight: FontWeight.w700,
            fontSize: 18,
            color: isDarkTheme ? Colors.white : Colors.black,
          ),
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: EdgeInsets.only(left: 4.w, right: 4.w, bottom: 1.h),
        child: Column(
          children: [
            Expanded(
              child: ListView(
                shrinkWrap: true,
                children: [
                  SizedBox(height: 2.h),
                  Stack(
                    alignment: Alignment.bottomCenter,
                    clipBehavior: Clip.none,
                    children: [
                      Center(
                        child: Container(
                          height: 20.h,
                          width: 90.w,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            image: DecorationImage(
                              image: NetworkImage(widget.app.icon),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: -10.h,
                        child: Column(
                          children: [
                            Container(
                              height: 10.h,
                              width: 30.w,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                image: DecorationImage(
                                  image: NetworkImage(
                                    widget.app.icon,
                                  ),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            SizedBox(height: 1.h),
                            reusableText(
                              text: widget.app.name,
                              style: TextStyle(
                                fontFamily: 'Tajawal',
                                fontWeight: FontWeight.w900,
                                fontSize: 18,
                                color:
                                    isDarkTheme ? Colors.white : Colors.black,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                  Padding(
                    padding: EdgeInsets.all(2.w),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Icon(
                              Icons.file_download_outlined,
                              size: 10.sp,
                            ),
                            SizedBox(width: 1.w),
                            reusableText(
                              text: widget.app.views,
                              style: TextStyle(
                                fontFamily: 'Tajawal',
                                fontWeight: FontWeight.w400,
                                fontSize: 12,
                                color:
                                    isDarkTheme ? Colors.white : Colors.black,
                              ),
                            ),
                          ],
                        ),
                        Container(
                          height: 4.h,
                          width: 0.5.w,
                          color: Colors.grey,
                        ),
                        reusableText(
                          text: widget.app.sizeFile,
                          textDirection: TextDirection.ltr,
                          style: TextStyle(
                            fontFamily: 'Tajawal',
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            color:
                                isDarkTheme ? Colors.white : Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                  reusableText(
                    text: "وصف",
                    style: TextStyle(
                      fontFamily: 'Tajawal',
                      fontWeight: FontWeight.w700,
                      fontSize: 14,
                      color: isDarkTheme ? Colors.white : Colors.black,
                    ),
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  reusableText(
                      text: widget.app.description,
                      style: const TextStyle(
                        fontFamily: 'Tajawal',
                        fontWeight: FontWeight.w400,
                        fontSize: 12,
                        color: AppColor.grey,
                      ),
                      maxLines: 4,
                      textAlign: TextAlign.justify),
                  SizedBox(
                    height: 2.h,
                  ),
                  reusableText(
                    text: "الإصدار",
                    style: TextStyle(
                      fontFamily: 'Tajawal',
                      fontWeight: FontWeight.w700,
                      fontSize: 14,
                      color: isDarkTheme ? Colors.white : Colors.black,
                    ),
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4.w),
                    child: reusableText(
                      text: widget.app.version,
                      style: const TextStyle(
                        fontFamily: 'Tajawal',
                        fontWeight: FontWeight.w400,
                        fontSize: 12,
                        color: AppColor.grey,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  reusableText(
                    text: "آخر تحديث",
                    style: TextStyle(
                      fontFamily: 'Tajawal',
                      fontWeight: FontWeight.w700,
                      fontSize: 14,
                      color: isDarkTheme ? Colors.white : Colors.black,
                    ),
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4.w),
                    child: reusableText(
                      text: widget.app.date,
                      style: const TextStyle(
                        fontFamily: 'Tajawal',
                        fontWeight: FontWeight.w400,
                        fontSize: 12,
                        color: AppColor.grey,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                ],
              ),
            ),
            reusableMaterialButton(
              isDarkTheme: isDarkTheme,
              function: () => EasyLauncher.url(
                url: widget.app.apkUrl,
              ),
              width:  80.w,
              height: 8.h,
              text: "تثبيت",
              borderColor: AppColor.primaryColor,
              color: AppColor.primaryColor,
            ),
          ],
        ),
      ),
    );
  }
}
