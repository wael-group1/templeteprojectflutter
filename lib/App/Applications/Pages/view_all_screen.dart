import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:servizio/widget/buildApplicationWidget.dart';
import 'package:sizer/sizer.dart';

// Model
import '../../Home/models/app_model.dart';

// Constant
import '../../../constant/appColor.dart';

// Cubit
import '../../Paint/Cubit/cubit.dart';

class ViewAllScreen extends StatelessWidget {
  final String title;
  final List<App> apps;

  const ViewAllScreen({required this.title, required this.apps});

  @override
  Widget build(BuildContext context) {
    var isDarkTheme = BlocProvider.of<ThemeCubit>(context).isDark;

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(
          title,
          style: TextStyle(
            fontFamily: 'Tajawal',
            fontWeight: FontWeight.w700,
            fontSize: 14,
            color: isDarkTheme ? Colors.white : Colors.black,
          ),
        ),
        actions: [
          IconButton(
            onPressed: () => Navigator.pop(context),
            icon: Icon(
              Icons.arrow_forward,
              color: isDarkTheme ? Colors.white : AppColor.primaryColor,
            ),
          ),
        ],
      ),
      body: ListView.separated(
        padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 2.h),
        itemBuilder: (context, index) => BuildAppWidget(app: apps[index]),
        itemCount: apps.length,
        separatorBuilder: (context, index) => SizedBox(height: 1.h),
      ),
    );
  }
}
