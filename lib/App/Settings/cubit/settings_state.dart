import 'package:flutter/material.dart';
import 'package:equatable/equatable.dart';
import '../model/settings_response.dart';

@immutable
abstract class SettingsState extends Equatable {}

class SettingsInitState extends SettingsState {
  @override
  List<Object?> get props => [];
}

class SettingsLoadingState extends SettingsState {
  @override
  List<Object?> get props => [];
}

class SettingsSuccessState extends SettingsState {
  final SettingsResponse settings;

  SettingsSuccessState(this.settings);

  @override
  List<Object?> get props => [settings];
}

class SettingsErrorState extends SettingsState {
  @override
  List<Object?> get props => [];
}