import 'package:bloc/bloc.dart';

// State
import '../../../SharedPreferences/SharedPreferencesHelper.dart';
import 'settings_state.dart';

// Model
import '../model/settings_response.dart';

// Network
import '../../../Apis/Network.dart';
import '../../../constant/urls.dart';

class SettingsCubit extends Cubit<SettingsState> {
  SettingsCubit() : super(SettingsInitState());

  getSettings() async {
    emit(SettingsLoadingState());
    try {
      final response = await Network.postData(
        url: Urls.info,
        data: {
          "key": AppSharedPreferences.getToken,
          "udid": AppSharedPreferences.getUdid,
          "token": "3Z-ALL998128-HOPIJkL-IO9779"
        },
      );
      final settings = settingsResponseFromJson(response.data);
      emit(SettingsSuccessState(settings));
    } catch (e) {
      emit(SettingsErrorState());
    }
  }
}
