import 'dart:convert';

SettingsResponse settingsResponseFromJson(String str) => SettingsResponse.fromJson(json.decode(str));

String settingsResponseToJson(SettingsResponse data) => json.encode(data.toJson());

class SettingsResponse {
  final String title;
  final String description;
  final String icon;
  final String webSite;
  final String snapchat;
  final String tikTok;
  final String youTube;
  final String twitter;
  final String instagram;
  final String telegram;
  final String whatsapp;

  SettingsResponse({
    required this.title,
    required this.description,
    required this.icon,
    required this.webSite,
    required this.snapchat,
    required this.tikTok,
    required this.youTube,
    required this.twitter,
    required this.instagram,
    required this.telegram,
    required this.whatsapp,
  });

  factory SettingsResponse.fromJson(Map<String, dynamic> json) => SettingsResponse(
    title: json["Title"],
    description: json["Description"],
    icon: json["Icon"],
    webSite: json["WebSite"],
    snapchat: json["Snapchat"],
    tikTok: json["TikTok"],
    youTube: json["YouTube"],
    twitter: json["Twitter"],
    instagram: json["Instagram"],
    telegram: json["Telegram"],
    whatsapp: json["Whatsapp"],
  );

  Map<String, dynamic> toJson() => {
    "Title": title,
    "Description": description,
    "Icon": icon,
    "WebSite": webSite,
    "Snapchat": snapchat,
    "TikTok": tikTok,
    "YouTube": youTube,
    "Twitter": twitter,
    "Instagram": instagram,
    "Telegram": telegram,
    "Whatsapp": whatsapp,
  };
}
