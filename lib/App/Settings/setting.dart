import 'package:easy_url_launcher/easy_url_launcher.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:servizio/App/Paint/Cubit/cubit.dart';
import 'package:servizio/App/Settings/cubit/settings_cubit.dart';
import 'package:servizio/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:servizio/componant.dart';
import 'package:servizio/constant/appColor.dart';
import 'package:servizio/constant/constimage.dart';
import 'package:shimmer_animation/shimmer_animation.dart';
import 'package:sizer/sizer.dart';

import '../../widget/try_again.dart';
import 'cubit/settings_state.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({super.key});

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  void initState() {
    super.initState();
    context.read<SettingsCubit>().getSettings();
  }

  @override
  Widget build(BuildContext context) {
    var isDarkTheme = BlocProvider.of<ThemeCubit>(context).isDark;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            'الاعدادات',
            style: TextStyle(
              fontFamily: 'Tajawal',
              fontWeight: FontWeight.w700,
              fontSize: 18,
              color: isDarkTheme ? Colors.white : Colors.black,
            ),
          ),
          automaticallyImplyLeading: false,
          actions: [
            IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(
                Icons.arrow_forward,
                size: 8.w,
                color: Theme.of(context).iconTheme.color!,
              ),
            ),
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Center(
                child: Container(
                  height: 18.h,
                  width: 30.w,
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(AppImageAsset.qrcode),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 2.h),
              reusableText(
                maxLines: 2,
                text: "الكود ينتهي في : ${AppSharedPreferences.getExpiryDate}",
                style: TextStyle(
                  fontFamily: 'Tajawal',
                  fontWeight: FontWeight.w400,
                  fontSize: 18,
                  color: isDarkTheme ? Colors.white : Colors.black,
                ),
              ),
              BlocBuilder<SettingsCubit, SettingsState>(
                builder: (context, state) {
                  if (state is SettingsLoadingState) {
                    return Expanded(
                      child: GridView.builder(
                        padding: EdgeInsets.only(
                          top: 4.h,
                          right: 2.w,
                          left: 2.w,
                        ),
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          mainAxisSpacing: 5.w,
                          crossAxisSpacing: 5.w,
                          childAspectRatio: 3,
                        ),
                        itemBuilder: (context, index) => _LoadingInfoItem(),
                        itemCount: 8,
                      ),
                    );
                  }
                  if (state is SettingsSuccessState) {
                    final settings = state.settings;
                    return Expanded(
                      child: GridView(
                        padding: EdgeInsets.only(
                          top: 4.h,
                          right: 2.w,
                          left: 2.w,
                        ),
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          mainAxisSpacing: 5.w,
                          crossAxisSpacing: 5.w,
                          childAspectRatio: 3,
                        ),
                        children: [
                          _InfoItem(
                            title: 'موقعنا على الانترنت',
                            url: settings.webSite,
                            icon: const Opacity(
                              opacity: 0.8,
                              child: Icon(
                                Icons.public,
                                color: AppColor.primaryColor,
                              ),
                            ),
                          ),
                          _InfoItem(
                            title: 'قناتنا على يوتيوب',
                            url: settings.youTube,
                            icon: Image.asset(AppImageAsset.yotube),
                          ),
                          _InfoItem(
                            title: 'حسابنا على تيك توك',
                            url: settings.tikTok,
                            icon: Image.asset(AppImageAsset.tiktok),
                          ),
                          _InfoItem(
                            title: 'حسابنا على تويتر',
                            url: settings.twitter,
                            icon: Image.asset(AppImageAsset.twitter),
                          ),
                          _InfoItem(
                            title: 'حسابنا على سناب شات',
                            url: settings.snapchat,
                            icon: const Icon(
                              Icons.snapchat,
                              color: AppColor.primaryColor,
                            ),
                          ),
                          _InfoItem(
                            title: 'حسابنا على انستغرام',
                            url: settings.instagram,
                            icon: Image.asset(AppImageAsset.instagram),
                          ),
                          _InfoItem(
                            title: 'حسابنا على واتساب',
                            url: settings.whatsapp,
                            icon: Image.asset(AppImageAsset.watsapp),
                          ),
                          _InfoItem(
                            title: 'حسابنا على تلغرام',
                            url: settings.telegram,
                            icon: const Opacity(
                              opacity: 0.7,
                              child: Icon(
                                Icons.telegram_outlined,
                                color: AppColor.primaryColor,
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  } else {
                    return TryAgainUi(
                      () => context.read<SettingsCubit>().getSettings(),
                    );
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _InfoItem extends StatelessWidget {
  final String title;
  final String url;
  final Widget icon;

  const _InfoItem({
    required this.title,
    required this.url,
    required this.icon,
  });

  @override
  Widget build(BuildContext context) {
    var isDarkTheme = BlocProvider.of<ThemeCubit>(context).isDark;

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
      ),
      child: InkWell(
        onTap: () => EasyLauncher.url(url: url),
        borderRadius: BorderRadius.circular(8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.all(1.w),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: isDarkTheme ? Colors.white : Colors.black,
                ),
              ),
              child: icon,
            ),
            SizedBox(width: 4.w),
            SizedBox(
              width: 25.w,
              child: Text(
                title,
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
                style: TextStyle(
                  color: isDarkTheme ? Colors.white : Colors.black,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _LoadingInfoItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Shimmer(
          child: CircleAvatar(
            radius: 4.w,
            backgroundColor: Colors.grey.shade300,
          ),
        ),
        SizedBox(width: 4.w),
        Shimmer(
          child: Container(
            width: 25.w,
            height: 3.h,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.grey.shade300,
            ),
          ),
        ),
      ],
    );
  }
}
