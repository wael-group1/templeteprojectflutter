class Urls {
  static const baseUrl = 'https://ibda3-android.com';
  static const login = '$baseUrl/api.php';
  static const getSections = '$baseUrl/Sections.php';
  static const getApps = '$baseUrl/3z.php';
  static const notifications = '$baseUrl/api_Notifications.php';
  static const banners = '$baseUrl/banner_api.php';
  static const info = '$baseUrl/info.php';
}