class AppImageAsset {

  static const String rootbackground = "assets/background";

  static const String rootimage = "assets/images";

  static const String cheldren = "$rootimage/cheldren.png";
  static const String games = "$rootimage/games.png";
  static const String crousal = "$rootimage/crousal.png";
  static const String appImage1 = "$rootimage/appImage1.png";
  static const String appImage2 = "$rootimage/appImage2.png";
  static const String appImage3 = "$rootimage/appImage3.png";
  static const String appImage4 = "$rootimage/appImage4.png";
  static const String appDetails1 = "$rootimage/appDetails1.png";
  static const String appDetails2 = "$rootimage/appDetails2.jpg";
  static const String appDetails3 = "$rootimage/appDetails3.jpg";
  static const String appDetails4 = "$rootimage/appDetails4.jpg";
  static const String yotube = "$rootimage/yotube.png";
  static const String tiktok = "$rootimage/tiktok.png";
  static const String twitter = "$rootimage/twitter.png";
  static const String snapchat = "$rootimage/snapchat.png";
  static const String instagram = "$rootimage/instagram.png";
  static const String watsapp = "$rootimage/watsapp.png";
  static const String telegram = "$rootimage/telegram.png";
  static const String drawing = "$rootimage/drawing.png";
  static const String qrcode = "$rootimage/qrcode.png";
}
