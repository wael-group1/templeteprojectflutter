
import 'package:flutter/material.dart';

class AppColor 
{
  static const primaryColor = Color(0xff00653A);
  static const grey = Color.fromRGBO(184, 184, 184, 1);
}
