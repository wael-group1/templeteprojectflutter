// import 'dart:async';
// import 'package:dio/dio.dart';

// class DioHelper {
//   static late Dio dio;
// // http://s984348237.onlinehome.us/api/
//   static init() {
//     dio = Dio(
//       BaseOptions(
//         baseUrl: 'https://api.openbrewerydb.org/',
//         receiveDataWhenStatusError: true,

//       ),
//     );
//   }

//   static Future<Response> getData({
//      String? url,
//     Map<String, dynamic>? query,
//     String ?token,
//     Map<String, dynamic> ?data,
//   }) async
//   {
//     // dio.options.headers = {
//     //   // 'Content-Type': 'multipart/form-data; boundary=<calculated when request is sent>',
//     //   // 'Content-Length': '<calculated when request is sent>',
//     //   // 'Host': '<calculated when request is sent>',
//     //   // 'User-Agent': 'PostmanRuntime/7.32.2',
//     //   // 'Accept': '*/*',
//     //   // 'Accept-Encoding': 'gzip, deflate, br',
//     //   // 'Connection': 'keep-alive',
//     //   // 'Authorization': token??'',

//     // };
//     return await dio.get(url!, queryParameters: query!,);
//   }

//   static Future<Response>postData(
//   {
//     required String url,
//     Map<String, dynamic> ?query,
//     required FormData ?data,

//   })async
// {
//   return await dio.post(url,queryParameters: query,data: data);
// }
//   static Future<Response>postLogout(
//       {
//         required String url,
//        required String ?token,
//       })async
//   {
//     dio.options.headers={
//       'Authorization':token??'',
//     };
//     return await dio.post(url);
//   }
//   static Future<Response> putData({
//     required String url,
//     Map<String, dynamic> ?query,
//     required Map<String, dynamic>data,
//     String ?token,
//   }) async
//   {
//     dio.options.headers = {
//       'Content-Type': 'multipart/form-data; boundary=<calculated when request is sent>',
//       'Content-Length': '<calculated when request is sent>',
//       'Host': '<calculated when request is sent>',
//       'User-Agent': 'PostmanRuntime/7.32.2',
//       'Accept': '*/*',
//       'Accept-Encoding': 'gzip, deflate, br',
//       'Connection': 'keep-alive',
//       'Authorization': token ?? '',
//     };
//     return await dio.put(url, queryParameters: query, data: data);
//   }
//   static Future<Response> getWithToken({
//      String? url,
//      String? token
//   }) async {
//     dio.interceptors.add(InterceptorsWrapper(
//       onRequest: (options, handler) {
//         options.headers['Authorization'] = 'Bearer $token';
//         return handler.next(options);
//       },
//     ));

//     try {
//       final response = await dio.get(url!);
//       return response;
//     } catch (e) {
//       throw e;
//     }
//   }

// }