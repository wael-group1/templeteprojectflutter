 import 'package:flutter/material.dart';
import 'package:servizio/animations/scale-transation-route.dart';
class General{
  
     static Future<void> routingPage(BuildContext context, Widget page) async {
    Navigator.of(context). push( ScaleTransationRoute(page: page));
  }

  static Future<void> removeUntilRoutingPage(BuildContext context, Widget page) async {
    Navigator.pushAndRemoveUntil(context, ScaleTransationRoute(page: page),(route)=>false);
  }
}