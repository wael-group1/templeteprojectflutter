// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';

import 'constant/appColor.dart';

Widget reusableSizedBox(double height, double width) => SizedBox(
      height: height,
      width: width,
    );

Widget MyText(text, double sizeText, color, isBold) {
  return Text(
    text,
    style: TextStyle(
        color: color,
        fontSize: sizeText,
        fontWeight: isBold == true ? FontWeight.bold : null),
    textAlign: TextAlign.center,
  );
}

Widget reusableText({
  void Function()? function,
  required String text,
  double fontSize = 24.0,
  FontWeight fontWeight = FontWeight.w700,
  int maxLines = 1,
  TextOverflow overFlowType = TextOverflow.ellipsis,
  TextAlign? textAlign,
  TextDirection? textDirection,
  TextStyle? style,
}) =>
    GestureDetector(
      onTap: function,
      child: Text(
        text,
        textAlign: textAlign,
        textDirection: textDirection,
        style: style,
        maxLines: maxLines,
        overflow: overFlowType,
      ),
    );

Widget reusableMaterialButton({
  required void Function()? function,
  required bool isDarkTheme,
  required double width,
  required double height,
  required String text,
  Color? color,
  Color? borderColor = Colors.transparent,
}) =>
    MaterialButton(
      shape: RoundedRectangleBorder(
        borderRadius: const BorderRadius.all(Radius.circular(8)),
        side: BorderSide(color: borderColor!),
      ),
      color: color,
      minWidth: width,
      height: height,
      onPressed: function,
      child: reusableText(
        text: text,
        style: TextStyle(
          fontFamily: 'Tajawal',
          fontWeight: FontWeight.w400,
          fontSize: 14,
          color: color == null
              ? isDarkTheme
                  ? Colors.white
                  : AppColor.primaryColor
              : Colors.white,
        ),
      ),
    );

Widget reusableTextForm({
  required TextEditingController controller,
  required TextInputType keyboardType,
  required bool isDarkTheme,
  required BuildContext context,
  bool isVisible = false,
  bool readonly = false,
  double radius = 0.0,
  void Function()? functionSuffix,
  IconData? iconSuffix,
  Icon? iconPrefix,
  int maxLines = 1,
  void Function()? onTap,
  String? Function(String?)? validate,
  String? hintText,
  required String labelText,
}) =>
    TextFormField(
      controller: controller,
      keyboardType: keyboardType,
      obscureText: isVisible,
      maxLines: maxLines,
      onTap: onTap,
      readOnly: readonly,
      style: TextStyle(color: isDarkTheme ? Colors.white : Colors.black),
      decoration: InputDecoration(
        alignLabelWithHint: true,
        label: Text(
          labelText,
          style: const TextStyle(
            fontFamily: 'Tajawal',
            fontWeight: FontWeight.w400,
            fontSize: 14,
            color: AppColor.grey,
          ),
        ),
        prefixIcon: iconPrefix,
        enabled: true,
        hintText: hintText,
        suffixIcon: IconButton(
          onPressed: functionSuffix,
          icon: Icon(iconSuffix),
        ),
        border: OutlineInputBorder(
          borderSide: const BorderSide(color: AppColor.grey),
          borderRadius: BorderRadius.circular(10),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: AppColor.grey),
          borderRadius: BorderRadius.circular(10),
        ),
      ),
      validator: validate,
      // onTapOutside: (event) => FocusScope.of(context).unfocus(),
    );
